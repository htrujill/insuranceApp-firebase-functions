const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.pushToHelpers = functions.database.ref('/requests/{uuid_requests}/helpers/{uuid_helper}').onCreate(event => {
	var helper = event.data.val();
	var uuid_requests = event.params.uuid_requests;
	var uuid_helper = event.params.uuid_helper;
	var body = "A user needs your assistance!";
	var registrationToken = "";
	var title = "Hey!";

	admin.database().ref('/helpers').once('value', function(snapshot) {
		var helpers = snapshot.val();

		snapshot.forEach( function(element) {
			if (element.key === uuid_helper) {
				registrationToken = element.child("tokenDevice").val();
			}
		});

		const payload = { 
			"notification": {
				"title":title,
				"body": body,
				"sound": "default",
				"click_action": "FCM_PLUGIN_ACTIVITY"
			},
			"data": {
				"title": title, 
				"body": body, 
				"sound": "default"
			}
		};
		const options = { priority: "high" };

		if (registrationToken !== "") {
			return admin.messaging().sendToDevice(registrationToken, payload, options).then(function(response) {
				console.log("Successfully sent message:", response);
			}).catch(function(error) {
				console.log("Error sending message:", error);
			});
		}else {
			console.log("no registrationToken");
		}

		return snapshot.val();
	});

});


exports.pushNotification = functions.database.ref('/requests/{uuid_requests}').onWrite( event => {

	var request = event.data.val();
	var uuid_requests = event.params.uuid_requests;
	var body = "";
	var registrationToken = "";
	var title = "You have a new notification!";

	if (request["status"] === "Open") {
		console.log("Request created by user");
		registrationToken = request["helper"]["tokenDevice"];
		body = "Your have a new request!"; 
	}
	if (request["status"] === "Assigned") {
		console.log("Request accepted by helper");
		registrationToken = request["user"]["tokenDevice"];
		body = "Your request has been accepted!"; 
	}
	if (request["status"] === "Canceled") {
		console.log("Canceled");
		if (request["lastmodified"] === "helper") {
			console.log("Request Canceled by helper");
			registrationToken = request["user"]["tokenDevice"];
			title = "Sorry...";
			body = "Your request has been canceled by the helper!"; 
		}else if (request["lastmodified"] === "user") {
			console.log("Request canceled by user");
			registrationToken = request["helper"]["tokenDevice"];
			title = "Ups...";
			body = "This request has been candeled by the user!"; 
		}
	}

	if (request["status"] === "Closed") {
		registrationToken = request["user"]["tokenDevice"];
		console.log("Request closed by helper");
		body = "Your request has been closed, please rate our app!"; 
	}

	const payload = { 
		"notification":{
			"title":title,
			"body": body,
			"sound":"default",
			"click_action":"FCM_PLUGIN_ACTIVITY"
		},
		"data": {
			"title": title, 
			"body": body, 
			"sound": "default",
			"status": request["status"]
		}
	};
	const options = { priority: "high" };

	if (registrationToken !== "") {
		return admin.messaging().sendToDevice(registrationToken, payload, options).then(function(response) {
			console.log("Successfully sent message:", response);
		}).catch(function(error) {
			console.log("Error sending message:", error);
		});
	}else {
		console.log("no registrationToken");
	}
});

function getHelpers(){
	var helpers = admin.database().ref('/helpers').once('value').then(function(snapshot) {
		console.log(snapshot);
		return snapshot.val();
	});
	return helpers;
}